﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using DMCrafts.WebB.Models;
using Microsoft.AspNetCore.Hosting;

namespace DMCrafts.WebB.Services
{
    public class JsonFileProductService
    {
        private IWebHostEnvironment WebHostEnvironment { get; }

        // ctor - passing in IWebHostEnvironment service so we can get the path to products.json
        public JsonFileProductService(IWebHostEnvironment webHostEnvironment) =>
            WebHostEnvironment = webHostEnvironment;

        // file path used in GetProducts and UpdateDaveScore 
        private string JsonFileName =>
            Path.Combine(WebHostEnvironment.WebRootPath, "data", "products.json");

        // get json text and convert to a list
        public IEnumerable<Product> GetProducts()
        {
            // C#8 using declaration 
            using var jsonFileReader = File.OpenText(JsonFileName);
            return JsonSerializer.Deserialize<Product[]>
            (jsonFileReader.ReadToEnd(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                });
        }

        public void UpdateDaveScore(string productId)
        {
            var products = GetProducts();
            var product = products.First(x => x.Id == productId);

            product.DaveScore += 1;

            using var outputStream = File.OpenWrite(JsonFileName);
            JsonSerializer.Serialize(
                new Utf8JsonWriter(outputStream, new JsonWriterOptions
                {
                    SkipValidation = true,
                    Indented = true
                }),
                products
            );
        }
    }
}
