﻿//using System.Collections.Generic;
//using DMCrafts.WebSite.Models;
//using DMCrafts.WebSite.Services;
//using Microsoft.AspNetCore.Mvc;

//namespace DMCrafts.WebSite.Controllers
//{
//    [Route("[controller]")]
//    [ApiController]
//    public class ProductsController : ControllerBase
//    {
//        public JsonFileProductService ProductService { get; }

//        public ProductsController(JsonFileProductService productService)
//        {
//            ProductService = productService;
//        }

//        [HttpGet]
//        public IEnumerable<Product> Get()
//        {
//            // default is to return json (do in startup)
//            return ProductService.GetProducts();
//        }

//        // https://localhost:44341/products/rate?ProductId=jenlooper-cactus&rating=5

//        [HttpGet]
//        [Route("Rate")]
//        public ActionResult Get(
//            [FromQuery]string productId,
//            [FromQuery]int rating)
//        {
//            ProductService.AddRating(productId, rating);

//            // returns a 200
//            return Ok();
//        }
//    }
//}