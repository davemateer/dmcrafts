﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Text.Json;
using DMCrafts.WebSite.Models;
using Microsoft.AspNetCore.Hosting;

namespace DMCrafts.WebSite.Services
{
    public class JsonFileProductService
    {
        public IWebHostEnvironment WebHostEnvironment { get; }

        // so we can get the path to products.json
        // passed in
        public JsonFileProductService(IWebHostEnvironment webHostEnvironment) =>
            WebHostEnvironment = webHostEnvironment;

        // get the file path and name to load
        private string JsonFileName =>
            Path.Combine(
                WebHostEnvironment.WebRootPath, "data", "products.json");

        // get json text and convert to a list
        public IEnumerable<Product> GetProducts()
        {
            // C#8 using declaration 
            using var jsonFileReader = File.OpenText(JsonFileName);
            return JsonSerializer.Deserialize<Product[]>
            (jsonFileReader.ReadToEnd(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                });
        }

        public void AddRating(string productId, int rating)
        {
            var products = GetProducts();

            var product = products.First(x => x.Id == productId);

            if (product.Ratings is null)
                product.Ratings = new int[] { rating };
            else
            {
                var ratings = product.Ratings.ToList();
                ratings.Add(rating);
                product.Ratings = ratings.ToArray();
            }

            using var outputStream = File.OpenWrite(JsonFileName);
            JsonSerializer.Serialize(
                new Utf8JsonWriter(outputStream, new JsonWriterOptions
                {
                    SkipValidation = true,
                    Indented = true
                }),
                products
            );
        }
    }
}
